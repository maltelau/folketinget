
import os
import collections
import re
import glob

prev = collections.Counter()
prevfiles = []
prevstage = ""


# with open("data/missing_pdfs.txt", "w") as _:
#     pass

for stage in ['metadata', 'pdf', 'txt', 'segmented', 'tidy']:
    if stage == "metadata":
        with open("data/pdf_list.txt") as f:
            files = [url.split("/")[-1] for url in f.read().split("\n")]
    else:
        files = [os.path.basename(filename) for filename in glob.glob(os.path.join("data", stage, "*"))]


    
    years = collections.Counter([int(filename[:4])
                                 for filename in files
                                 if filename])
    nfiles = sum([y for y in years.values()])
    if nfiles == 0:
        continue
    
    print(f"########\n{stage} has {nfiles} files")

    # how many new / missing files?

    if stage == "metadata":
        pass

    else:
        # how many new / missing files?
        compare = {year: years[year] - value for (year, value) in prev.items()}

        years_ok = [year for year, value in compare.items() if value == 0]
        if not all([v == 0 for v in compare.values()]):
            print(f"\nDONE: {years_ok}")

            # uncomment to show which years are missing
            # for year, value in compare.items():
            #     if value != 0:
            #         print(f"NOT DONE: {year}: {value}")

            
            testfiles = [os.path.splitext(filename)[0] for filename in files]
            for filename in prevfiles:
                if not os.path.splitext(filename)[0] in testfiles:
                    if not filename:
                        continue
                    
                    # with open("data/missing_pdfs.txt", "a") as missing_pdfs:
                    #     samling, meeting, _ = re.split("_", filename)
                    #     # os.remove(os.path.join("data", prevstage, filename))
                    #     missing_pdfs.write(f"https://www.folketingstidende.dk/samling/{samling}/salen/{meeting}/{filename}\n")
                        
                    # print(f"MISSING: {filename}")

    prevstage = stage
    prev = years
    prevfiles = files
